var orderCount =0;


function takeOrder(topping, crustType)
{
	orderCount++;
	console.log("Order:", crustType ,"pizza topped with", topping);
}


function getSubTotal(itemCount)
{
	return itemCount * 7.5;
}

function getTax()
{
	return getSubTotal(orderCount) * 0.06; 
}

function getTotal()
{
	return getSubTotal(orderCount) + getTax();
}

takeOrder("eggs", "thin");
takeOrder("cheese", "thin");
takeOrder("vegetables", "thin");

console.log("Price:" ,getSubTotal(orderCount));

console.log("Price after tax: ", getTotal());
