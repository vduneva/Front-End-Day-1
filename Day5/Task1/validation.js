$(document).ready(function()
{

			$.ajax({
				url:"http://jsonplaceholder.typicode.com/users",
				method: 'GET',
				dataType:"json",
				success: function(data) {
					$('#datatable').dataTable({
						data : data, 
						columns:
						[
						{ 'data' : 'name' },
						{ 'data' : 'username' },
						{ 'data' : 'email' },
						{ 'data' : 'address.city' },
						{ 'data' : 'address.street' },
						{ 'data' : 'phone' },
						{ 'data' : 'company.name'},
						]
						
					});
				}
				
			});
		})


