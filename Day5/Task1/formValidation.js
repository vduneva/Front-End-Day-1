$(document).ready(function()
{
	$("#registerForm").validate({
		rules:
		{
			fullName:
			{
				required:true,
				minlength:6
			},

			username:
			{
				required:true,
				minlength:3
			},
			email:
			{
				required:true,
			},
			city:
			{
				required:true,
				minlength:2
			},

			phone:
			{
				required:true,
				minlength:10
			},

			companyName:
			{
				required:true,
				minlength:3
			}

		},
		messages:{
			fullName:
			{
				required:"Please enter your full name",
				fullName: "Please enter valid full name - between 6 and 50 symbols"
			},
			username:
			{
				required:"Please enter your username",
				minlength: "Please enter valid username  - at least 3 characters"
			},
			email:
			{
				required:"Please enter your email"
			},
			city:
			{
				required:"Please enter your city",
				minlength: "Please enter valid city  - at least 2 characters"
			},
			
			phone:
			{
				required:"Please enter your phone",
				minlength: "Please enter valid phone  - at least 10 characters"
			},
			companyName:
			{
				required:"Please enter your company name",
				minlength: "Please enter valid company name  - at least 3 characters",
				maxlength: "Please enter valid company name  - max characters 50"
			}

		},
		
		submitHandler: function (f) {    
			$.ajax({ 
				type: "POST", 
				data: $(f).serialize(),
				url: "https://jsonplaceholder.typicode.com/users", 
				success: function (data) {
					show_stack_custom("success");
					console.log(data);
				},
				error: function (data) {
					show_stack_custom("error");
					console.log(data);
				}                        
			});
		}
	})

})


function show_stack_custom(type) {
    var opts = {
        title: "Over Here",
        text: "Check me out.",
        addclass: "stack-custom"
    };
    switch (type) {
    case 'error':
        opts.title = "Oh No";
        opts.text = "Watch out for validation!";
        opts.type = "error";
        break;
    case 'success':
        opts.title = "Gzzzzzzz";
        opts.text = "You've registered!";
        opts.type = "success";
        break;
    }
    new PNotify(opts);
}